# Using the Ubuntu image
FROM python:3.8

# Install our Python dependencies
RUN pip install mkdocs markdown-include mkdocs-material mkdocs-material-extensions
